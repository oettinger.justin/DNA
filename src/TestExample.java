import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author toolbox
 * A example JUnit Test class
 */
public class TestExample {
	/**
	 *	Double checks that true is, in fact, true
	 */
	@Test
	public void sanityCheck() {
		assertTrue(true);
	}
	
	/**
	 *	Double checks that false is, in fact, false
	 */
	@Test
	public void sanityCheck2() {
		assertFalse(false);
	}	
}
