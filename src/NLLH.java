
public class NLLH {

	public static void main(String[] args) {
		for(int i = 1; i < 11; i++) {
			for(int j = 0; j < 10; j++) {
				String bob = maker(i);
				String spliceThis = splicee(j);
				StringStrand tester = new StringStrand(bob);
				double start = System.nanoTime();
				tester.cutAndSplice("CGA", spliceThis);
				double time = (System.nanoTime() - start) / 1.0e9;
				System.out.println(time);
			}
		}
	}
	private static String maker(int b) {
		String add = "CGA";
		String strand = "";
		int count = 0;
		for(int i = 0; i < b; i++) {
			strand = strand + add;
			count = count + add.length();
		}
		for(int i = count; i < 2000; i++) {
			strand = strand + "z";
		}
		return strand;
	}
	
	private static String splicee(int s) {
		String add = "CGA";
		for(int i = 0; i < s; i++) {
			add = add + "CGA";
		}
		return add;
	}
}
