
public class LinkStrand implements IDnaStrand {
	
	private class Node{
		String info;
		Node next;
		public Node(String s) {
			info = s;
			next = null;
		}
	}
	
	private Node myFirst, myLast;
	private long mySize;
	private int myAppends;
	private Node searchHold;
	private int previousSearch;
	private int indexHold;
	
	public LinkStrand() {
		this("");
	}
	
	public LinkStrand(String s) {
		initialize(s);
	}
	
	public LinkStrand(Node first, Node last, int apps, int size) {
		myFirst = first;
		myLast = last;
		myAppends = apps;
		mySize = size;
	}

	@Override
	public long size() {
		return mySize;
	}

	@Override
	public void initialize(String source) {
		Node node = new Node(source);
		myFirst = myLast = node;
		mySize = source.length();
		myAppends = 0;
	}

	@Override
	public IDnaStrand getInstance(String source) {
		return new LinkStrand(source);
	}

	@Override
	public IDnaStrand append(String dna) {
		Node node = new Node(dna);
		myLast.next = node;
		myLast = node;
		mySize = mySize + dna.length();
		myAppends++;
		return this;
	}
	
	@Override
	public String toString() {
		Node node = myFirst;
		StringBuilder bob = new StringBuilder();
		while(node != null) {
			bob.append(node.info);
			node = node.next;
		}
		return bob.toString();
	}

	@Override
	public IDnaStrand reverse() {
		Node node = myFirst;
		Node hold = null;
		Node newList = null;
		Node last = null;
		int appends = -1; int size = 0;
		while(node != null) {
			newList = new Node(flip(node));
			newList.next = hold;
			if(hold == null) {
				last = newList;
			}
			hold = newList;
			size = size + newList.info.length();
			appends++;
			node = node.next;
		}
		LinkStrand revList = new LinkStrand(newList, last, appends, size);
		return revList;
	}
	
	private String flip(Node n) {
		StringBuilder revDNA = new StringBuilder();
		revDNA.append(n.info);
		revDNA.reverse();
		return revDNA.toString();
	}

	@Override
	public int getAppendCount() {
		return myAppends;
	}

	@Override
	public char charAt(int index) {
		if(index >= mySize || index <= -1) {
			throw new IndexOutOfBoundsException("Index is out of bounds.");
		}
		if(index == previousSearch + 1) {
			indexHold++;
			if(indexHold == searchHold.info.length()) {
				searchHold = searchHold.next;
				indexHold = 0;
			}
			return searchHold.info.charAt(indexHold);
		}
		else {
			int count = 0;
			int dex = 0;
			Node list = myFirst;
			while(count != index) {
				count++;
				dex++;
				if(dex >= list.info.length()) {
					dex = 0;
					list = list.next;
				}
			}
			if(dex == list.info.length() - 1) {
				searchHold = list.next;
				indexHold = -1;
			}
			else {
				searchHold = list;
				indexHold = dex;
			}
			previousSearch = index;
			return list.info.charAt(dex);
		}
	}

}
