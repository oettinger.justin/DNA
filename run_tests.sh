#!/bin/bash
cd src/
FAILURE=0
echo "Test files detected: $(echo Test*.java)"
for TESTFILE in Test*.java
do
  echo "----------------------------------------"
  echo "Compiling $TESTFILE..."
  javac -cp .:$JUNIT_PATH $TESTFILE || export FAILURE=1 echo "Failed to compile"
done
for TESTFILE in Test*.class
do
  echo "----------------------------------------"
  echo "Running $TESTFILE..."
  java -cp .:$JUNIT_PATH:$(pwd) org.junit.runner.JUnitCore ${TESTFILE%.*} || export FAILURE=1 echo "Failed."
done
echo "Failure status: $FAILURE"
exit $FAILURE